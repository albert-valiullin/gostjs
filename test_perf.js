var iter = 10;
function perfEcb(printFunc) {
    var context = {};
    modes.initEcb15(masterKey15, context);
    var t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptEcb(context, MULT_TABLE, []);
    }
    var t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initEcb89(masterKey89, context);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptEcb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));
}

function perfCtr(printFunc) {
    var context = {};
    modes.initCtr15(masterKey15, context, ivCtr15, blockLen15);
    var t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCtr(context, MULT_TABLE, []);
    }
    var t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCtr15(masterKey15, context, ivCtr15, blockLen15/2);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCtr(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCtr15(masterKey15, context, ivCtr15, blockLen15/4);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCtr(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCtr89(masterKey89, context, ivCtr89, blockLen89);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCtr(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCtr89(masterKey89, context, ivCtr89, blockLen89/2);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCtr(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCtr89(masterKey89, context, ivCtr89, blockLen89/4);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCtr(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));
}

function perfCbc(printFunc) {
    var context = {};
    modes.initCbc15(masterKey15, context, MULT_TABLE.slice(0, 16));
    var t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCbc(context, MULT_TABLE, []);
    }
    var t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCbc15(masterKey15, context, MULT_TABLE.slice(0, 32));
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCbc(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCbc15(masterKey15, context, MULT_TABLE.slice(0, 64));
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCbc(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCbc89(masterKey89, context, MULT_TABLE.slice(0, 8));
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCbc(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCbc89(masterKey89, context, MULT_TABLE.slice(0, 16));
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCbc(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCbc89(masterKey89, context, MULT_TABLE.slice(0, 32));
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCbc(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));
}

function perfOfbS(printFunc) {
    var context = {};
    modes.initOfb15(masterKey15, context, ivOfb15, blockLen15);
    var t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    var t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initOfb15(masterKey15, context, ivOfb15, blockLen15/2);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initOfb15(masterKey15, context, ivOfb15, blockLen15/4);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initOfb89(masterKey89, context, ivOfb89, blockLen89);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initOfb89(masterKey89, context, ivOfb89, blockLen89/2);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initOfb89(masterKey89, context, ivOfb89, blockLen89/4);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));
}

function perfOfbM(printFunc) {
    var context = {};
    modes.initOfb15(masterKey15, context, MULT_TABLE.slice(0, 16), blockLen15);
    var t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    var t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initOfb15(masterKey15, context, MULT_TABLE.slice(0, 32), blockLen15);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initOfb15(masterKey15, context, MULT_TABLE.slice(0, 64), blockLen15);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initOfb89(masterKey89, context, MULT_TABLE.slice(0, 8), blockLen89);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initOfb89(masterKey89, context, MULT_TABLE.slice(0, 16), blockLen89);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initOfb89(masterKey89, context, MULT_TABLE.slice(0, 32), blockLen89);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptOfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));
}

function perfCfbS(printFunc) {
    var context = {};
    modes.initCfb15(masterKey15, context, ivCfb15, blockLen15);
    var t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    var t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCfb15(masterKey15, context, ivCfb15, blockLen15/2);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCfb15(masterKey15, context, ivCfb15, blockLen15/4);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCfb89(masterKey89, context, ivCfb89, blockLen89);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCfb89(masterKey89, context, ivCfb89, blockLen89/2);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCfb89(masterKey89, context, ivCfb89, blockLen89/4);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));
}

function perfCfbM(printFunc) {
    var context = {};
    modes.initCfb15(masterKey15, context, MULT_TABLE.slice(0, 16), blockLen15);
    var t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    var t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCfb15(masterKey15, context, MULT_TABLE.slice(0, 32), blockLen15);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCfb15(masterKey15, context, MULT_TABLE.slice(0, 64), blockLen15);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCfb89(masterKey89, context, MULT_TABLE.slice(0, 8), blockLen89);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCfb89(masterKey89, context, MULT_TABLE.slice(0, 16), blockLen89);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initCfb89(masterKey89, context, MULT_TABLE.slice(0, 32), blockLen89);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.encryptCfb(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));
}

function perfImit(printFunc) {
    var context = {};
    modes.initImit15(masterKey15, context, blockLen15);
    var t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.imit(context, MULT_TABLE, []);
    }
    var t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initImit15(masterKey15, context, blockLen15/2);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.imit(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initImit15(masterKey15, context, blockLen15/4);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.imit(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initImit89(masterKey89, context, blockLen89);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.imit(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initImit89(masterKey89, context, blockLen89/2);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.imit(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));

    context = {};
    modes.initImit89(masterKey89, context, blockLen89/4);
    t0 = performance.now();
    for (var i = 0; i < iter * 1024*1024 / MULT_TABLE.length; i++) {
        modes.imit(context, MULT_TABLE, []);
    }
    t1 = performance.now();
    console.log(iter * 1000/(t1-t0));
}