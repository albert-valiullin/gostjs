/**
 * Алгоритм шифрования "Магма"
 * @constructor
 */
function Gost89() {
    var blockLen = 8;
    /**
     * @returns {number} Длина блока в байтах.
     */
    this.getBlockLen = function() {
        return blockLen;
    };
    /**
     * Преобразование массива байт в число. Преобразуются 4 элемента массива начиная с начального индекса.
     *
     * @param {number[]} indata входной массив байт для преобразования
     * @param {number} startIdx начальный индекс для входного массива
     * @returns {number} целое число размером 32 бит
     */
    var byteArrToNum32bit = function(indata, startIdx) {
        return ( (indata[startIdx + 3]) | (indata[startIdx + 2] << 8) | (indata[startIdx + 1] << 16) | (indata[startIdx] << 24));
    };
    /**
     * Преобразование целого числа в массив байт.
     * Результатом являются 4 элемента массива начиная со стартового индекса в выходном массиве.
     *
     * @param {number} input целое число размером 32 бит
     * @param {number[]} outdata выходной массив байт для результата преобразования
     * @param outStartIdx начальный индекс для выходного массива
     */
    var num32bitToByteArr = function(input, outdata, outStartIdx) {
        for (var i = 0; i < 4; ++i) {
            outdata[outStartIdx + 3 - i] = ( ( input >> (8 * i) ) & 0x000000ff );
        }
    };
    /**
     * Нелинейное биективное преобразование множества двоичных векторов. (Таблица подстановки)
     *
     * @type {number[][]}
     * @const
     */
    var PI = [
        [0xc, 0x4, 0x6, 0x2, 0xa, 0x5, 0xb, 0x9, 0xe, 0x8, 0xd, 0x7, 0x0, 0x3, 0xf, 0x1],
        [0x6, 0x8, 0x2, 0x3, 0x9, 0xa, 0x5, 0xc, 0x1, 0xe, 0x4, 0x7, 0xb, 0xd, 0x0, 0xf],
        [0xb, 0x3, 0x5, 0x8, 0x2, 0xf, 0xa, 0xd, 0xe, 0x1, 0x7, 0x4, 0xc, 0x9, 0x6, 0x0],
        [0xc, 0x8, 0x2, 0x1, 0xd, 0x4, 0xf, 0x6, 0x7, 0x0, 0xa, 0x5, 0x3, 0xe, 0x9, 0xb],
        [0x7, 0xf, 0x5, 0xa, 0x8, 0x1, 0x6, 0xd, 0x0, 0x9, 0x3, 0xe, 0xb, 0x4, 0x2, 0xc],
        [0x5, 0xd, 0xf, 0x6, 0x9, 0x2, 0xc, 0xa, 0xb, 0x7, 0x8, 0x1, 0x4, 0x3, 0xe, 0x0],
        [0x8, 0xe, 0x2, 0x5, 0x6, 0x9, 0x1, 0xc, 0xf, 0x4, 0xb, 0x0, 0xd, 0xa, 0x3, 0x7],
        [0x1, 0x7, 0xe, 0xd, 0x0, 0x5, 0x8, 0x3, 0x4, 0xf, 0xa, 0x6, 0x9, 0xc, 0xb, 0x2]
    ];
    /**
     * Начальные позиции раундовых ключей при зашифровании.
     *
     * @type {number[]}
     */
    var ROUND_KEY_ENC_POS = [
        0, 4, 8, 12, 16, 20, 24, 28, 0, 4, 8, 12, 16, 20, 24, 28, 0, 4, 8, 12, 16, 20, 24, 28, 28, 24, 20, 16, 12, 8, 4, 0
    ];
    /**
     * Начальные позиции раундовых ключей при расшифровании.
     *
     * @type {number[]}
     */
    var ROUND_KEY_DEC_POS = [
        0, 4, 8, 12, 16, 20, 24, 28, 28, 24, 20, 16, 12, 8, 4, 0, 28, 24, 20, 16, 12, 8, 4, 0, 28, 24, 20, 16, 12, 8, 4, 0
    ];
    /**
     * Преобазование t.
     * Подстановка, осуществляемая с помощью матрицы {@link PI}.
     *
     * @param {number} a полублок - целое число, отображающее двоичный вектор длиной 32 бит
     * @param {function} [print] функция логирования
     * @returns {number} результат преобразования - целое число, отображающее двоичный вектор длиной 32 бит
     */
    var funcT = function(a, print) {
        var res = 0;
        var a16 = new Uint16Array([a >> 16, a]);

        res ^= PI[0][a16[1] & 0x000f];
        res ^= PI[1][(a16[1] & 0x00f0) >> 4 ] << 4;
        res ^= PI[2][(a16[1] & 0x0f00) >> 8 ] << 8;
        res ^= PI[3][(a16[1] & 0xf000) >> 12] << 12;
        res ^= PI[4][(a16[0] & 0x000f) >> 0 ] << 16;
        res ^= PI[5][(a16[0] & 0x00f0) >> 4 ] << 20;
        res ^= PI[6][(a16[0] & 0x0f00) >> 8 ] << 24;
        res ^= PI[7][(a16[0] & 0xf000) >> 12] << 28;

        if (print) {
            print("funcT: a: ", a);
            print("funcT: output: ", res);
        }
        return res;
    };
    /**
     * Преобразование g.
     * Основная фунция для преобразования полублока, используемая в сети Фейсталя.
     *
     * @param {number} a полублок - целое число, отображающее двоичный вектор длиной 32 бит
     * @param {number} k раундовый ключ - целое число, отображающее двоичный вектор длиной 32 бит
     * @param {function} [print] функция логирования
     * @returns {number} результат преобразования - целое число, отображающее двоичный вектор длиной 32 бит
     */
    var funcG = function(a, k, print) {
        var c = a + k;
        var tmp = funcT(c, print);

        var r = (tmp << 11) | (tmp >>> 21);

        if (print) {
            print("funcG: a: ", a);
            print("funcG: k: ", k);
            print("funcG: output: ", r);
        }
        return r;
    };
    /**
     * Преобразование G.
     * Раундовое преобразование двух полублоков.
     *
     * @param {Uint32Array} a массив из двух элементов - два полублока
     * @param {number} k раундовый ключ - целое число, отображающее двоичный вектор длиной 32 бит
     * @param {function} [print] функция логирования
     */
    var round = function(a, k, print) {
        if (print) {
            print("round: input a1: ", a[1]);
            print("round: input a0: ", a[0]);
            print("round: k: ", k);
        }

        var aTmp = a[0];
        var tmp = funcG(a[0], k, print);

        a[0] = a[1] ^ tmp;
        a[1] = aTmp;

        if (print) {
            print("round: output a1: ", a1);
            print("round: output a0: ", a0);
        }
    };
    /**
     * Преобразование G*.
     * Последнее раундовое преобразование двух полублоков.
     *
     * @param {Uint32Array} a массив из двух элементов - два полублока
     * @param {number} k раундовый ключ - целое число, отображающее двоичный вектор длиной 32 бит
     * @param {function} [print] функция логирования
     */
    var lastRound = function(a, k, print) {
        if (print) {
            print("lastRound: input a1: ", a[1]);
            print("lastRound: input a0: ", a[0]);
            print("lastRound: k: ", k);
        }

        a[1] ^= funcG(a[0], k, print);

        if (print) {
            print("lastRound: output a1: ", a[1]);
            print("lastRound: output a0: ", a[0]);
        }
    };
    /**
     * Базовое преобразование блока.
     *
     * @param {number[]} indata входной массив байт - блок данных для преобразования
     * @param {number[]} outdata выходной массив байт - блок данных для результата преобразования
     * @param {number[]} key входной массив байт - мастер-ключи
     * @param {number[]} keyIndex начальные позиции раундовых ключей
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var cryptBlock = function(indata, outdata, key, keyIndex, print) {
        var a = new Uint32Array([byteArrToNum32bit(indata, blockLen / 2), byteArrToNum32bit(indata, 0)]);

        if (print) {
            print("cryptBlock: indata a1:", a[1]);
            print("cryptBlock: indata a0:", a[0]);
        }

        for (var i = 0; i < 31; i++) {
            round(a, byteArrToNum32bit(key, keyIndex[i]), print);
        }

        lastRound(a, byteArrToNum32bit(key, keyIndex[31]), print);

        if (print) {
            print("cryptBlock: outdata a1:", a1);
            print("cryptBlock: outdata a0:", a0);
        }

        num32bitToByteArr(a[1], outdata, 0);
        num32bitToByteArr(a[0], outdata, 4);
        return 0;
    };
    /**
     * Зашифрование блока.
     *
     * @param {number[]} plainText входной массив байт - блок данных для преобразования
     * @param {number[]} cipherText выходной массив байт - блок данных для результата преобразования
     * @param {number[]} key входной массив байт - мастер-ключи
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.encrypt = function(plainText, cipherText, key, print) {
        if (!plainText || !cipherText || !key) {
            if (print) {
                print("encrypt89: internal error!");
            }
            return -1;
        }

        if (cryptBlock(plainText, cipherText, key, ROUND_KEY_ENC_POS, print)) {
            return -1;
        }

        if (print) {
            print("encrypt89: plain text: ", plainText);
            print("encrypt89: cipher text: ", cipherText);
        }
        return 0;
    };
    /**
     * Расшифрование блока.
     *
     * @param {number[]} cipherText входной массив байт - блок данных для преобразования
     * @param {number[]} plainText выходной массив байт - блок данных для результата преобразования
     * @param {number[]} key входной массив байт - мастер-ключи
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.decrypt = function(cipherText, plainText, key, print) {
        if (!cipherText || !plainText || !key) {
            if (print) {
                print("decrypt89: internal error!");
            }
            return -1;
        }

        if (cryptBlock(cipherText, plainText, key, ROUND_KEY_DEC_POS, print)) {
            return -1;
        }

        if (print) {
            print("decrypt89: cipher text: ", cipherText);
            print("decrypt89: plain text: ", plainText);
        }
        return 0;
    };
}