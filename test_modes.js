var modes = new Modes();
var blockLen15 = 16;
var blockLen89 = 8;

var masterKey15 = [
    0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef
];
var plainText15 = [
    0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x00, 0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x88,
    0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xee, 0xff, 0x0a,
    0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xee, 0xff, 0x0a, 0x00,
    0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xee, 0xff, 0x0a, 0x00, 0x11
];
var masterKey89 = [
    0xff, 0xee, 0xdd, 0xcc, 0xbb, 0xaa, 0x99, 0x88,
    0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
    0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7,
    0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff
];
var plainText89 = [
    0x92, 0xde, 0xf0, 0x6b, 0x3c, 0x13, 0x0a, 0x59,
    0xdb, 0x54, 0xc7, 0x04, 0xf8, 0x18, 0x9d, 0x20,
    0x4a, 0x98, 0xfb, 0x2e, 0x67, 0xa8, 0x02, 0x4c,
    0x89, 0x12, 0x40, 0x9b, 0x17, 0xb5, 0x7e, 0x41
];
function checkArrayEquals(arr1, arr2) {
    if (arr1.length != arr2.length) {
        return false;
    }
    for (var i = 0; i < arr1.length; i++) {
        if (arr1[i] !== arr2[i] ) {
            //console.log(arr1[i], " ", arr2[i]);
            return false;
        }
    }
    return true;
}

/////////////////////////       ECB          //////////////////////////////////////
var controlEcb15 = [
    0x7f, 0x67, 0x9d, 0x90, 0xbe, 0xbc, 0x24, 0x30, 0x5a, 0x46, 0x8d, 0x42, 0xb9, 0xd4, 0xed, 0xcd,
    0xb4, 0x29, 0x91, 0x2c, 0x6e, 0x00, 0x32, 0xf9, 0x28, 0x54, 0x52, 0xd7, 0x67, 0x18, 0xd0, 0x8b,
    0xf0, 0xca, 0x33, 0x54, 0x9d, 0x24, 0x7c, 0xee, 0xf3, 0xf5, 0xa5, 0x31, 0x3b, 0xd4, 0xb1, 0x57,
    0xd0, 0xb0, 0x9c, 0xcd, 0xe8, 0x30, 0xb9, 0xeb, 0x3a, 0x02, 0xc4, 0xc5, 0xaa, 0x8a, 0xda, 0x98,
    0x5d, 0x2e, 0x0e, 0xf7, 0x18, 0x12, 0xe3, 0x9f, 0x76, 0xd7, 0x4c, 0x27, 0x58, 0x7e, 0x0b, 0xde
];
var controlEcb89 = [
    0x2b, 0x07, 0x3f, 0x04, 0x94, 0xf3, 0x72, 0xa0,
    0xde, 0x70, 0xe7, 0x15, 0xd3, 0x55, 0x6e, 0x48,
    0x11, 0xd8, 0xd9, 0xe9, 0xea, 0xcf, 0xbc, 0x1e,
    0x7c, 0x68, 0x26, 0x09, 0x96, 0xc6, 0x7e, 0xfb,
    0x8e, 0xc0, 0xf1, 0xc5, 0x6a, 0xf3, 0x0d, 0x28
];
function testEcb(printFunc) {
    var context = {};
    var result = [];
    var out = [];
    modes.initEcb15(masterKey15, context);

    modes.encryptEcb(context, plainText15, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlEcb15)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlEcb15);
    }

    modes.decryptEcb(context, out, result);
    if (printFunc) prettyPrint(result, printFunc);
    if (!checkArrayEquals(result, plainText15)) {
        throw new Error("Результат: \n" + result + ";\n Ожидается: \n" + plainText15);
    }

    // GOST89
    context = {};
    result = [];
    out = [];
    modes.initEcb89(masterKey89, context);

    modes.encryptEcb(context, plainText89, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlEcb89)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlEcb89);
    }

    modes.decryptEcb(context, out, result);
    if (printFunc) prettyPrint(result, printFunc);
    if (!checkArrayEquals(result, plainText89)) {
        throw new Error("Результат: \n" + result + ";\n Ожидается: \n" + plainText89);
    }
}
/////////////////////////       CBC          //////////////////////////////////////
var ivCbc15 = [
    0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xce, 0xf0, 0xa1, 0xb2, 0xc3, 0xd4, 0xe5, 0xf0, 0x01, 0x12,
    0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89, 0x90, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19
];
var ivCbc89 = [
    0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef,
    0x23, 0x45, 0x67, 0x89, 0x0a, 0xbc, 0xde, 0xf1,
    0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef, 0x12
];
var controlCbc15 = [
    0x68, 0x99, 0x72, 0xd4, 0xa0, 0x85, 0xfa, 0x4d, 0x90, 0xe5, 0x2e, 0x3d, 0x6d, 0x7d, 0xcc, 0x27,
    0x28, 0x26, 0xe6, 0x61, 0xb4, 0x78, 0xec, 0xa6, 0xaf, 0x1e, 0x8e, 0x44, 0x8d, 0x5e, 0xa5, 0xac,
    0xfe, 0x7b, 0xab, 0xf1, 0xe9, 0x19, 0x99, 0xe8, 0x56, 0x40, 0xe8, 0xb0, 0xf4, 0x9d, 0x90, 0xd0,
    0x16, 0x76, 0x88, 0x06, 0x5a, 0x89, 0x5c, 0x63, 0x1a, 0x2d, 0x9a, 0x15, 0x60, 0xb6, 0x39, 0x70,
    0xfa, 0xe6, 0x41, 0xba, 0xae, 0x5b, 0xbf, 0x5a, 0x27, 0x50, 0xe1, 0x8e, 0xef, 0x51, 0x93, 0xca
];
var controlCbc89 = [
    0x96, 0xd1, 0xb0, 0x5e, 0xea, 0x68, 0x39, 0x19,
    0xaf, 0xf7, 0x61, 0x29, 0xab, 0xb9, 0x37, 0xb9,
    0x50, 0x58, 0xb4, 0xa1, 0xc4, 0xbc, 0x00, 0x19,
    0x20, 0xb7, 0x8b, 0x1a, 0x7c, 0xd7, 0xe6, 0x67,
    0xe8, 0x14, 0xab, 0x75, 0xa4, 0x74, 0xe8, 0x94
];
function testCbc(printFunc) {
    var context = {};
    var result = [];
    var out = [];
    modes.initCbc15(masterKey15, context, ivCbc15);

    modes.encryptCbc(context, plainText15, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlCbc15)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlCbc15);
    }

    modes.initCbc15(masterKey15, context, ivCbc15);

    modes.decryptCbc(context, out, result);
    if (printFunc) prettyPrint(result, printFunc);
    if (!checkArrayEquals(result, plainText15)) {
        throw new Error("Результат: \n" + result + ";\n Ожидается: \n" + plainText15);
    }

    // GOST89
    context = {};
    result = [];
    out = [];
    modes.initCbc89(masterKey89, context, ivCbc89);

    modes.encryptCbc(context, plainText89, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlCbc89)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlCbc89);
    }

    modes.initCbc89(masterKey89, context, ivCbc89);

    modes.decryptCbc(context, out, result);
    if (printFunc) prettyPrint(result, printFunc);
    if (!checkArrayEquals(result, plainText89)) {
        throw new Error("Результат: \n" + result + ";\n Ожидается: \n" + plainText89);
    }
}

/////////////////////////       CTR          //////////////////////////////////////
var ivCtr15 = [
    0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xce, 0xf0
];
var ivCtr89 = [
    0x12, 0x34, 0x56, 0x78
];
var controlCtr15 = [
    0xf1, 0x95, 0xd8, 0xbe, 0xc1, 0x0e, 0xd1, 0xdb, 0xd5, 0x7b, 0x5f, 0xa2, 0x40, 0xbd, 0xa1, 0xb8,
    0x85, 0xee, 0xe7, 0x33, 0xf6, 0xa1, 0x3e, 0x5d, 0xf3, 0x3c, 0xe4, 0xb3, 0x3c, 0x45, 0xde, 0xe4,
    0xa5, 0xea, 0xe8, 0x8b, 0xe6, 0x35, 0x6e, 0xd3, 0xd5, 0xe8, 0x77, 0xf1, 0x35, 0x64, 0xa3, 0xa5,
    0xcb, 0x91, 0xfa, 0xb1, 0xf2, 0x0c, 0xba, 0xb6, 0xd1, 0xc6, 0xd1, 0x58, 0x20, 0xbd, 0xba, 0x73
];
var controlCtr89 = [
    0x4e, 0x98, 0x11, 0x0c, 0x97, 0xb7, 0xb9, 0x3c,
    0x3e, 0x25, 0x0d, 0x93, 0xd6, 0xe8, 0x5d, 0x69,
    0x13, 0x6d, 0x86, 0x88, 0x07, 0xb2, 0xdb, 0xef,
    0x56, 0x8e, 0xb6, 0x80, 0xab, 0x52, 0xa1, 0x2d
];
function testCtr(printFunc) {
    var context = {};
    var result = [];
    var out = [];
    modes.initCtr15(masterKey15, context, ivCtr15, blockLen15);

    modes.cryptCtr(context, plainText15, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlCtr15)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlCtr15);
    }

    modes.initCtr15(masterKey15, context, ivCtr15, blockLen15);

    modes.cryptCtr(context, out, result);
    if (printFunc) prettyPrint(result, printFunc);
    if (!checkArrayEquals(result, plainText15)) {
        throw new Error("Результат: \n" + result + ";\n Ожидается: \n" + plainText15);
    }

    // GOST89
    context = {};
    result = [];
    out = [];
    modes.initCtr89(masterKey89, context, ivCtr89, blockLen89);

    modes.cryptCtr(context, plainText89, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlCtr89)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlCtr89);
    }

    modes.initCtr89(masterKey89, context, ivCtr89, blockLen89);

    modes.cryptCtr(context, out, result);
    if (printFunc) prettyPrint(result, printFunc);
    if (!checkArrayEquals(result, plainText89)) {
        throw new Error("Результат: \n" + result + ";\n Ожидается: \n" + plainText89);
    }
}

/////////////////////////       OFB          //////////////////////////////////////
var ivOfb15 = [
    0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xce, 0xf0, 0xa1, 0xb2, 0xc3, 0xd4, 0xe5, 0xf0, 0x01, 0x12,
    0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89, 0x90, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19
];
var ivOfb89 = [
    0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef,
    0x23, 0x45, 0x67, 0x89, 0x0a, 0xbc, 0xde, 0xf1
];
var controlOfb15 = [
    0x81, 0x80, 0x0a, 0x59, 0xb1, 0x84, 0x2b, 0x24, 0xff, 0x1f, 0x79, 0x5e, 0x89, 0x7a, 0xbd, 0x95,
    0xed, 0x5b, 0x47, 0xa7, 0x04, 0x8c, 0xfa, 0xb4, 0x8f, 0xb5, 0x21, 0x36, 0x9d, 0x93, 0x26, 0xbf,
    0x66, 0xa2, 0x57, 0xac, 0x3c, 0xa0, 0xb8, 0xb1, 0xc8, 0x0f, 0xe7, 0xfc, 0x10, 0x28, 0x8a, 0x13,
    0x20, 0x3e, 0xbb, 0xc0, 0x66, 0x13, 0x86, 0x60, 0xa0, 0x29, 0x22, 0x43, 0xf6, 0x90, 0x31, 0x50
];
var controlOfb89 = [
    0xdb, 0x37, 0xe0, 0xe2, 0x66, 0x90, 0x3c, 0x83,
    0x0d, 0x46, 0x64, 0x4c, 0x1f, 0x9a, 0x08, 0x9c,
    0xa0, 0xf8, 0x30, 0x62, 0x43, 0x0e, 0x32, 0x7e,
    0xc8, 0x24, 0xef, 0xb8, 0xbd, 0x4f, 0xdb, 0x05
];
function testOfb(printFunc) {
    var context = {};
    var result = [];
    var out = [];
    modes.initOfb15(masterKey15, context, ivOfb15, blockLen15);

    modes.cryptOfb(context, plainText15, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlOfb15)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlOfb15);
    }

    modes.initOfb15(masterKey15, context, ivOfb15, blockLen15);

    modes.cryptOfb(context, out, result);
    if (printFunc) prettyPrint(result, printFunc);
    if (!checkArrayEquals(result, plainText15)) {
        throw new Error("Результат: \n" + result + ";\n Ожидается: \n" + plainText15);
    }

    // GOST89
    context = {};
    result = [];
    out = [];
    modes.initOfb89(masterKey89, context, ivOfb89, blockLen89);

    modes.cryptOfb(context, plainText89, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlOfb89)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlOfb89);
    }

    modes.initOfb89(masterKey89, context, ivOfb89, blockLen89);

    modes.cryptOfb(context, out, result);
    if (printFunc) prettyPrint(result, printFunc);
    if (!checkArrayEquals(result, plainText89)) {
        throw new Error("Результат: \n" + result + ";\n Ожидается: \n" + plainText89);
    }
}

/////////////////////////       CFB          //////////////////////////////////////
var ivCfb15 = [
    0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xce, 0xf0, 0xa1, 0xb2, 0xc3, 0xd4, 0xe5, 0xf0, 0x01, 0x12,
    0x23, 0x34, 0x45, 0x56, 0x67, 0x78, 0x89, 0x90, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19
];
var ivCfb89 = [
    0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef,
    0x23, 0x45, 0x67, 0x89, 0x0a, 0xbc, 0xde, 0xf1
];
var controlCfb15 = [
    0x81, 0x80, 0x0a, 0x59, 0xb1, 0x84, 0x2b, 0x24, 0xff, 0x1f, 0x79, 0x5e, 0x89, 0x7a, 0xbd, 0x95,
    0xed, 0x5b, 0x47, 0xa7, 0x04, 0x8c, 0xfa, 0xb4, 0x8f, 0xb5, 0x21, 0x36, 0x9d, 0x93, 0x26, 0xbf,
    0x79, 0xf2, 0xa8, 0xeb, 0x5c, 0xc6, 0x8d, 0x38, 0x84, 0x2d, 0x26, 0x4e, 0x97, 0xa2, 0x38, 0xb5,
    0x4f, 0xfe, 0xbe, 0xcd, 0x4e, 0x92, 0x2d, 0xe6, 0xc7, 0x5b, 0xd9, 0xdd, 0x44, 0xfb, 0xf4, 0xd1
];
var controlCfb89 = [
    0xdb, 0x37, 0xe0, 0xe2, 0x66, 0x90, 0x3c, 0x83,
    0x0d, 0x46, 0x64, 0x4c, 0x1f, 0x9a, 0x08, 0x9c,
    0x24, 0xbd, 0xd2, 0x03, 0x53, 0x15, 0xd3, 0x8b,
    0xbc, 0xc0, 0x32, 0x14, 0x21, 0x07, 0x55, 0x05
];
function testCfb(printFunc) {
    var context = {};
    var result = [];
    var out = [];
    modes.initCfb15(masterKey15, context, ivCfb15, blockLen15);

    modes.encryptCfb(context, plainText15, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlCfb15)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlCfb15);
    }

    modes.initCfb15(masterKey15, context, ivCfb15, blockLen15);

    modes.decryptCfb(context, out, result);
    if (printFunc) prettyPrint(result, printFunc);
    if (!checkArrayEquals(result, plainText15)) {
        throw new Error("Результат: \n" + result + ";\n Ожидается: \n" + plainText15);
    }

    // GOST89
    context = {};
    result = [];
    out = [];
    modes.initCfb89(masterKey89, context, ivCfb89, blockLen89);

    modes.encryptCfb(context, plainText89, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlCfb89)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlCfb89);
    }

    modes.initCfb89(masterKey89, context, ivCfb89, blockLen89);

    modes.decryptCfb(context, out, result);
    if (printFunc) prettyPrint(result, printFunc);
    if (!checkArrayEquals(result, plainText89)) {
        throw new Error("Результат: \n" + result + ";\n Ожидается: \n" + plainText89);
    }
}

/////////////////////////       Imit          //////////////////////////////////////
var controlK1_15 = [
    0x29, 0x7d, 0x82, 0xbc, 0x4d, 0x39, 0xe3, 0xca, 0x0d, 0xe0, 0x57, 0x32, 0x98, 0x15, 0x1d, 0xc7
];
var controlK2_15 = [
    0x52, 0xfb, 0x05, 0x78, 0x9a, 0x73, 0xc7, 0x94, 0x1b, 0xc0, 0xae, 0x65, 0x30, 0x2a, 0x3b, 0x8e
];
var controlK1_89 = [
    0x5f, 0x45, 0x9b, 0x33, 0x42, 0x52, 0x14, 0x24
];
var controlK2_89 = [
    0xbe, 0x8b, 0x36, 0x66, 0x84, 0xa4, 0x28, 0x48
];
var controlImit15 = [
    0x33, 0x6f, 0x4d, 0x29, 0x60, 0x59, 0xfb, 0xe3
];
var controlImit89 = [
    0x15, 0x4e, 0x72, 0x10
];
function testImit(printFunc) {
    var context = {};
    var out = [];
    modes.initImit15(masterKey15, context, blockLen15/2);

    modes.imit(context, plainText15, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlImit15)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlImit15);
    }

    // GOST89
    context = {};
    out = [];
    modes.initImit89(masterKey89, context, blockLen89/2);

    modes.imit(context, plainText89, out);
    if (printFunc) prettyPrint(out, printFunc);
    if (!checkArrayEquals(out, controlImit89)){
        throw new Error("Результат: \n" + out + ";\n Ожидается: \n" + controlImit89);
    }
}

///////////////////////////////////////////////////////////////////////////////
function testAll() {
    testEcb(console.log);
    testCbc(console.log);
    testCtr(console.log);
    testOfb(console.log);
    testCfb(console.log);
    testImit(console.log);
}
