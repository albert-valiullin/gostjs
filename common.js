function arrCopy(dstArr, srcArr, dstIdx, srcIdx, size) {
    for (var i = 0; i < size; i++) {
        dstArr[i+dstIdx] = srcArr[i+srcIdx];
    }
}
function pushArray(inArr, outArr) {
    for(var i = 0; i < inArr.length; i++) {
        outArr.push(inArr[i]);
    }
}
function fillPadding(dataArr, blockLength) {
    if (!dataArr || !blockLength || !dataArr.length) {
        return -1;
    }
    var resultArr = dataArr.slice(0);
    resultArr.push(1);

    var n = blockLength - (resultArr.length % blockLength);
    for (var i = 0; i < n; i++) {
        resultArr.push(0);
    }
    return resultArr;
}
function removePadding(dataArr) {
    if (!dataArr || !dataArr.length) {
        return -1;
    }
    var k;
    while((k = dataArr.pop()) === 0) {}

    if (k !== 1)
        return -1;

    return 0;
}
function prettyPrint(arr, printFunc) {
    var out = "[";
    for (var i = 0; i < arr.length; i++) {
        out += "0x"+arr[i].toString(16) + ", ";
        //out += arr[i].toString(16) + " ";
    }
    out += "]";
    printFunc(out);
}
function boolArray(byteArr) {
    var resArr = [];
    for (var i = 0; i < byteArr.length; i++) {
        var tmp = (byteArr[i] >>> 0).toString(2);
        if (tmp.length > 8) throw "Converting error";
        var tmpArr = [];
        for (var j = 0; j < 8 - tmp.length; j++) {
            tmpArr[j] = false;
        }
        for (var j = 0; j < tmp.length; j++) {
            tmpArr[j + 8 - tmp.length] = tmp.charAt(j) == "1";
        }
        resArr = resArr.concat(tmpArr);
    }
    return resArr;
}
function boolAr(byteArr) {
    var resArr = [];
    for (var i = 0; i < byteArr.length; i++) {
        var x = 0x80;
        for (var j = 0; j < 8; j++) {
            resArr[i*8 + j] = ((byteArr[i] & x) != 0);
            x >>= 1;
        }
    }
    return resArr;
}
