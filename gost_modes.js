/**
 * Режимы блочных шифров.
 * @constructor
 */
function Modes() {
/////////////////////////       ECB          //////////////////////////////////////
    var gost15 = new Gost15();
    var gost89 = new Gost89();
    var blockLen15 = 16;
    var blockLen89 = 8;

    var initEcb = function(key, ctx, algorithm, print) {
        if (!ctx || !key) {
            return -1;
        }
        ctx.printArray = print;

        ctx.encryptFunc = algorithm.encrypt;
        ctx.decryptFunc = algorithm.decrypt;
        ctx.blockLen = algorithm.getBlockLen();
        return 0;
    };
    /**
     * Инициализация параметров для шифрования в режиме ECB для алгоритма Кузнечик.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initEcb15 = function(key, ctx, print) {
        if (initEcb(key, ctx, gost15, print)) {
            return -1;
        }
        ctx.keys = [];
        return gost15.expandKey(key, ctx.keys, print);
    };
    /**
     * Инициализация параметров для шифрования в режиме ECB для алгоритма Магма.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initEcb89 = function(key, ctx, print) {
        if (initEcb(key, ctx, gost89, print)) {
            return -1;
        }
        ctx.keys = key.slice(0);
        return 0;
    };
    var cryptEcb = function(ctx, indata, outdata, cryptFunc) {
        if (!indata.length || (indata.length % ctx.blockLen)) {
            return -1;
        }

        for (var i = 0; i < (indata.length / ctx.blockLen); i++) {
            var inblock = indata.slice(i * ctx.blockLen, (i + 1) * ctx.blockLen);
            var outblock = [];
            cryptFunc(inblock, outblock, ctx.keys, ctx.printArray);

            pushArray(outblock, outdata);
        }
        return 0;
    };
    /**
     * Зашифрование в режиме ECB.
     *
     * @param {Object} ctx контекст
     * @param {number[]} indata входной массив байт - данные для шифрования
     * @param {number[]} outdata выходной массив байт - зашифрованные данные
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.encryptEcb = function(ctx, indata, outdata) {
        if (!ctx || !indata || !outdata) {
            return -1;
        }

        indata = fillPadding(indata, ctx.blockLen);

        return cryptEcb(ctx, indata, outdata, ctx.encryptFunc);
    };
    /**
     * Расшифрование в режиме ECB.
     *
     * @param {Object} ctx контекст
     * @param {number[]} indata входной массив байт - зашифрованные данные
     * @param {number[]} outdata выходной массив байт - расшифрованные данные
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.decryptEcb = function(ctx, indata, outdata) {
        if (!ctx || !indata || !outdata) {
            return -1;
        }

        cryptEcb(ctx, indata, outdata, ctx.decryptFunc);

        return removePadding(outdata);
    };

/////////////////////////       CBC          //////////////////////////////////////
    var initCbc = function(key, ctx, iv, algorithm, print) {
        if (!ctx || !key || !iv || !iv.length || (iv.length % algorithm.getBlockLen())) {
            return -1;
        }
        initEcb(key, ctx, algorithm, print);
        ctx.m = iv.length;
        ctx.iv = iv.slice(0);
        ctx.tempIV = [];
        ctx.nextIV = [];
        ctx.tmpblock = [];
    };
    /**
     * Инициализация параметров для шифрования в режиме ECB для алгоритма Кузнечик.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {number[]} iv массив байт - вектор инициализации (синхропосылка)
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initCbc15 = function(key, ctx, iv, print) {
        if (initCbc(key, ctx, iv, gost15, print)) {
            return -1;
        }
        ctx.keys = [];
        return gost15.expandKey(key, ctx.keys, print);
    };
    /**
     * Инициализация параметров для шифрования в режиме ECB для алгоритма Магма.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {number[]} iv массив байт - вектор инициализации (синхропосылка)
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initCbc89 = function(key, ctx, iv, print) {
        if (initCbc(key, ctx, iv, gost89, print)) {
            return -1;
        }
        ctx.keys = key.slice(0);
        return 0;
    };
    var packBlock = function(a, aLen, b, r, rLen, aIdx, bIdx, rIdx) {
        arrCopy(r, a, rIdx, aIdx, aLen);
        arrCopy(r, b, aLen, bIdx, rLen - aLen);
    };
    var encryptCbcBlock = function(inblock, outblock, ctx) {
        for (var j = 0; j < ctx.blockLen; j++) {
            ctx.tmpblock[j] = ctx.tempIV[j] ^ inblock[j];
        }
        ctx.encryptFunc(ctx.tmpblock, outblock, ctx.keys, ctx.printArray);

        packBlock(ctx.tempIV, ctx.m - ctx.blockLen, outblock, ctx.nextIV, ctx.m, ctx.blockLen, 0, 0);
        return 0;
    };
    var cryptCbc = function(ctx, indata, outdata, cryptFunc) {
        if (!indata.length || (indata.length % ctx.blockLen)) {
            return -1;
        }

        ctx.tempIV = ctx.iv.slice(0);

        for (var i = 0; i < (indata.length / ctx.blockLen); i++) {
            var inblock = indata.slice(i * ctx.blockLen, (i + 1) * ctx.blockLen);
            var outblock = [];

            cryptFunc(inblock, outblock, ctx);

            arrCopy(ctx.tempIV, ctx.nextIV, 0, 0, ctx.m);
            pushArray(outblock, outdata);
        }
        return 0;
    };
    var decryptCbcBlock = function(inblock, outblock, ctx) {
        ctx.decryptFunc(inblock, outblock, ctx.keys, ctx.printArray);

        for (var j = 0; j < ctx.blockLen; j++) {
            outblock[j] ^= ctx.tempIV[j];
        }
        packBlock(ctx.tempIV, ctx.m - ctx.blockLen, inblock, ctx.nextIV, ctx.m, ctx.blockLen, 0, 0);
        return 0;
    };
    /**
     * Зашифрование в режиме CBC.
     *
     * @param {Object} ctx контекст
     * @param {number[]} indata входной массив байт - данные для шифрования
     * @param {number[]} outdata выходной массив байт - зашифрованные данные
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.encryptCbc = function(ctx, indata, outdata) {
        if (!ctx || !indata || !outdata) {
            return -1;
        }

        indata = fillPadding(indata, ctx.blockLen);

        return cryptCbc(ctx, indata, outdata, encryptCbcBlock);
    };
    /**
     * Расшифрование в режиме CBC.
     *
     * @param {Object} ctx контекст
     * @param {number[]} indata входной массив байт - зашифрованные данные
     * @param {number[]} outdata выходной массив байт - расшифрованные данные
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.decryptCbc = function(ctx, indata, outdata) {
        if (!ctx || !indata || !outdata) {
            return -1;
        }

        cryptCbc(ctx, indata, outdata, decryptCbcBlock);

        return removePadding(outdata);
    };

/////////////////////////       CTR          //////////////////////////////////////
    var initCtr = function(key, ctx, iv, s, algorithm, print) {
        if (!ctx || !key || s > algorithm.getBlockLen()) {
            return -1;
        }
        ctx.encryptFunc = algorithm.encrypt;
        ctx.blockLen = algorithm.getBlockLen();
        ctx.s = s;
        ctx.tmpblock = [];
        ctx.counter = [];
        arrCopy(ctx.counter, iv, 0, 0, ctx.blockLen / 2);
        for (var i = ctx.blockLen / 2; i < ctx.blockLen; i++) {
            ctx.counter[i] = 0;
        }

        ctx.printArray = print;
    };
    /**
     * Инициализация параметров для шифрования в режиме CTR для алгоритма Кузнечик.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {number[]} iv массив байт - вектор инициализации (синхропосылка)
     * @param {number} s длина блоков для разбиения исходного текста
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initCtr15 = function(key, ctx, iv, s, print) {
        if (initCtr(key, ctx, iv, s, gost15, print)) {
            return -1;
        }
        ctx.keys = [];
        return gost15.expandKey(key, ctx.keys, print);
    };
    /**
     * Инициализация параметров для шифрования в режиме CTR для алгоритма Магма.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {number[]} iv массив байт - вектор инициализации (синхропосылка)
     * @param {number} s длина блоков для разбиения исходного текста
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initCtr89 = function(key, ctx, iv, s, print) {
        if (initCtr(key, ctx, iv, s, gost89, print)) {
            return -1;
        }
        ctx.keys = key.slice(0);
        return 0;
    };
    var incrementMod = function(value, size) {
        var lastIndex = size - 1;

        for (var i = 0; i < size; i++) {
            if (value[lastIndex - i] > 0xfe) {
                value[lastIndex - i] -= 0xfe;
            } else {
                value[lastIndex - i]++;
                break;
            }
        }
    };
    /**
     * Зашифрование\расшифрование в режиме CTR.
     *
     * @param {Object} ctx контекст
     * @param {number[]} indata входной массив байт - данные для шифрования
     * @param {number[]} outdata выходной массив байт - шифрованные данные
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.cryptCtr = function(ctx, indata, outdata) {
        if (!indata || !outdata || !ctx || !indata.length) {
            return -1;
        }

        for (var i = 0; i < Math.ceil(indata.length / ctx.s); i++) {
            ctx.encryptFunc(ctx.counter, ctx.tmpblock, ctx.keys, ctx.printArray);

            var inblock = indata.slice(i * ctx.s, (i + 1) * ctx.s);
            var outblock = [];

            var sBlockLen = (0 < (inblock.length % ctx.s)) ? (indata.length % ctx.s) : ctx.s;
            for (var j = 0; j < sBlockLen; j++) {
                outblock[j] = inblock[j] ^ ctx.tmpblock[j];
            }
            pushArray(outblock, outdata);
            incrementMod(ctx.counter, ctx.blockLen);
        }
        return 0;
    };

/////////////////////////       OFB          //////////////////////////////////////
    var initFb = function(key, ctx, iv, s, algorithm, print) {
        if (!ctx || !key || !s || s > algorithm.getBlockLen()) {
            return -1;
        }
        ctx.encryptFunc = algorithm.encrypt;
        ctx.blockLen = algorithm.getBlockLen();
        ctx.tmpblock = [];
        ctx.nextIV = [];
        ctx.m = iv.length;
        ctx.s = s;
        ctx.iv = [];
        arrCopy(ctx.iv, iv, 0, 0, iv.length);

        ctx.printArray = print;
    };
    /**
     * Инициализация параметров для шифрования в режиме OFB для алгоритма Кузнечик.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {number[]} iv массив байт - вектор инициализации (синхропосылка)
     * @param {number} s длина блоков для разбиения исходного текста
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initOfb15 = function(key, ctx, iv, s, print) {
        if (!iv.length || (iv.length % gost15.getBlockLen())) {
            return -1;
        }
        if (initFb(key, ctx, iv, s, gost15, print)) {
            return -1;
        }
        ctx.keys = [];
        return gost15.expandKey(key, ctx.keys, print);
    };
    /**
     * Инициализация параметров для шифрования в режиме OFB для алгоритма Магма.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {number[]} iv массив байт - вектор инициализации (синхропосылка)
     * @param {number} s длина блоков для разбиения исходного текста
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initOfb89 = function(key, ctx, iv, s, print) {
        if (!iv.length || (iv.length % gost89.getBlockLen())) {
            return -1;
        }
        if (initFb(key, ctx, iv, s, gost89, print)) {
            return -1;
        }
        ctx.keys = key.slice(0);
        return 0;
    };
    /**
     * Зашифрование\расшифрование в режиме OFB.
     *
     * @param {Object} ctx контекст
     * @param {number[]} indata входной массив байт - данные для шифрования
     * @param {number[]} outdata выходной массив байт - шифрованные данные
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.cryptOfb = function(ctx, indata, outdata) {
        if (!indata || !outdata || !ctx || !indata.length) {
            return -1;
        }

        for (var i = 0; i < Math.ceil(indata.length / ctx.s); i++) {
            ctx.encryptFunc(ctx.iv, ctx.tmpblock, ctx.keys, ctx.printArray);

            packBlock(ctx.iv, ctx.m - ctx.blockLen, ctx.tmpblock, ctx.nextIV, ctx.m, ctx.blockLen, 0, 0);

            arrCopy(ctx.iv, ctx.nextIV, 0, 0, ctx.m);
            var inblock = indata.slice(i * ctx.s, (i + 1) * ctx.s);
            var outblock = [];

            var sBlockLen = (0 < (inblock.length % ctx.s)) ? (indata.length % ctx.s) : ctx.s;
            for (var j = 0; j < sBlockLen; j++) {
                outblock[j] = inblock[j] ^ ctx.tmpblock[j];
            }
            pushArray(outblock, outdata);
        }
        return 0;
    };

/////////////////////////       CFB          //////////////////////////////////////
    /**
     * Инициализация параметров для шифрования в режиме CFB для алгоритма Кузнечик.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {number[]} iv массив байт - вектор инициализации (синхропосылка)
     * @param {number} s длина блоков для разбиения исходного текста
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initCfb15 = function(key, ctx, iv, s, print) {
        if (!iv.length || (iv.length < gost15.getBlockLen())) {
            return -1;
        }
        if (initFb(key, ctx, iv, s, gost15, print)) {
            return -1;
        }
        ctx.keys = [];
        return gost15.expandKey(key, ctx.keys, print);
    };
    /**
     * Инициализация параметров для шифрования в режиме CFB для алгоритма Магма.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {number[]} iv массив байт - вектор инициализации (синхропосылка)
     * @param {number} s длина блоков для разбиения исходного текста
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initCfb89 = function(key, ctx, iv, s, print) {
        if (!iv.length || (iv.length < gost89.getBlockLen())) {
            return -1;
        }
        if (initFb(key, ctx, iv, s, gost89, print)) {
            return -1;
        }
        ctx.keys = key.slice(0);
        return 0;
    };

    var cryptCfb = function(ctx, indata, outdata, isEncrypt) {
        if (!indata || !outdata || !ctx || !indata.length) {
            return -1;
        }

        for (var i = 0; i < Math.ceil(indata.length / ctx.s); i++) {
            ctx.encryptFunc(ctx.iv, ctx.tmpblock, ctx.keys, ctx.printArray);

            var inblock = indata.slice(i * ctx.s, (i + 1) * ctx.s);
            var outblock = [];

            var sBlockLen = (0 < (inblock.length % ctx.s)) ? (indata.length % ctx.s) : ctx.s;
            for (var j = 0; j < sBlockLen; j++) {
                outblock[j] = inblock[j] ^ ctx.tmpblock[j];
            }

            packBlock(ctx.iv, ctx.m - ctx.s, isEncrypt ? outblock : inblock, ctx.nextIV, ctx.m, ctx.s, 0, 0);
            arrCopy(ctx.iv, ctx.nextIV, 0, 0, ctx.m);

            pushArray(outblock, outdata);
        }
        return 0;
    };
    /**
     * Зашифрование в режиме CFB.
     *
     * @param {Object} ctx контекст
     * @param {number[]} indata входной массив байт - данные для шифрования
     * @param {number[]} outdata выходной массив байт - зашифрованные данные
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.encryptCfb = function(ctx, indata, outdata) {
        return cryptCfb(ctx, indata, outdata, true);
    };
    /**
     * Расшифрование в режиме CFB.
     *
     * @param {Object} ctx контекст
     * @param {number[]} indata входной массив байт - зашифрованные данные
     * @param {number[]} outdata выходной массив байт - расшифрованные данные
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.decryptCfb = function(ctx, indata, outdata) {
        return cryptCfb(ctx, indata, outdata, false);
    };

/////////////////////////       Imit          //////////////////////////////////////
    var expandImitKey = function(key, ctx) {
        if (!ctx || !key) {
            return -1;
        }
        ctx.tmpblock.splice(0, ctx.tmpblock.length);

        ctx.encryptFunc(ctx.tmpblock, ctx.r, ctx.keys, ctx.printArray);

        var r = ((ctx.r[0] & 0x80) == 0x80);

        shiftLeft(ctx.r);

        if (r) {
            for (var i = 0; i < ctx.blockLen; i++) {
                ctx.k1[i] = ctx.r[i] ^ ctx.b[i];
            }
        } else {
            arrCopy(ctx.k1, ctx.r, 0, 0, ctx.blockLen);
        }

        arrCopy(ctx.tmpblock, ctx.k1, 0, 0, ctx.blockLen);
        shiftLeft(ctx.tmpblock);

        if ((ctx.k1[0] & 0x80) == 0x80) {
            for (var i = 0; i < ctx.blockLen; i++) {
                ctx.k2[i] = ctx.tmpblock[i] ^ ctx.b[i];
            }
        } else {
            arrCopy(ctx.k2, ctx.tmpblock, 0, 0, ctx.blockLen);
        }
        return 0;
    };
    var shiftLeft = function(r) {
        var len = r.length;
        for (var i = 0; i < len - 1; i++) {
            r[i] <<= 1;
            r[i] &= 0xfe;
            r[i] |= ((r[i + 1] >> 7) & 0x1);
        }

        r[len - 1] <<= 1;
        r[len - 1] &= 0xfe;
    };
    var initImit = function(key, ctx, s, algorithm, print) {
        if (!ctx || !key || !s || s > algorithm.getBlockLen()) {
            return -1;
        }
        ctx.encryptFunc = algorithm.encrypt;
        ctx.blockLen = algorithm.getBlockLen();
        ctx.s = s;
        ctx.r = [];
        ctx.b = [];
        ctx.k1 = [];
        ctx.k2 = [];
        ctx.c = [];
        ctx.lastBlock = [];
        ctx.tmpblock = [];
        ctx.resimit = [];

        for (var i = 0; i < ctx.blockLen - 1; i++) {
            ctx.b[i] = 0;
        }
        ctx.lastBlockSize = 0;
        ctx.isFistBlock = 1;

        ctx.printArray = print;
    };
    /**
     * Инициализация параметров для режима выработки имитовставки для алгоритма Кузнечик.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {number} s длина блоков для разбиения исходного текста
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initImit15 = function(key, ctx, s, print) {
        if (initImit(key, ctx, s, gost15, print)) {
            return -1;
        }
        ctx.keys = [];
        gost15.expandKey(key, ctx.keys, print);
        ctx.b[blockLen15 - 1] = 0x87;

        expandImitKey(key, ctx);
        return 0;
    };
    /**
     * Инициализация параметров для режима выработки имитовставки для алгоритма Магма.
     *
     * @param {number[]} key массив байт - масстер-ключ
     * @param {Object} ctx контейнер контекста
     * @param {number} s длина блоков для разбиения исходного текста
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.initImit89 = function(key, ctx, s, print) {
        if (initImit(key, ctx, s, gost89, print)) {
            return -1;
        }
        ctx.keys = key.slice(0);
        ctx.b[blockLen89 - 1] = 0x1B;

        expandImitKey(key, ctx);
        return 0;
    };
    var padd = function(data, length, blockLen) {
        if (!data || !length) {
            return -1;
        }

        var paddingLen = blockLen - (length % blockLen);
        data[length] = 1;

        for (var i = 1; i < paddingLen; i++) {
            data[length + i] = 0;
        }
        return length + paddingLen;
    };
    /**
     * Выработка имитовставки.
     *
     * @param {Object} ctx контекст
     * @param {number[]} indata входной массив байт - исходные данные
     * @param {number[]} outdata выходной массив байт - имитовставка (MAC)
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.imit = function(ctx, indata, outdata) {
        if (!ctx || !indata || !indata.length || !outdata) {
            return -1;
        }

        for (var i = 0; i < Math.floor(indata.length / ctx.blockLen); i++) {
            var inblock = indata.slice(i * ctx.blockLen, (i + 1) * ctx.blockLen);

            if (ctx.isFistBlock) {
                arrCopy(ctx.lastBlock, inblock, 0, 0, ctx.blockLen);
                ctx.lastBlockSize = ctx.blockLen;
                ctx.isFistBlock = 0;

                continue;
            }

            for (var j = 0; j < ctx.blockLen; j++) {
                ctx.lastBlock[j] ^= ctx.c[j];
            }

            ctx.encryptFunc(ctx.lastBlock, ctx.c, ctx.keys, ctx.printArray);
            arrCopy(ctx.lastBlock, inblock, 0, 0, ctx.blockLen);
        }

        arrCopy(ctx.tmpblock, ctx.lastBlock, 0, 0, ctx.lastBlockSize);
        if (ctx.lastBlockSize != ctx.blockLen) {
            padd(ctx.tmpblock, ctx.lastBlockSize, ctx.blockLen);
        }

        for (var i = 0; i < ctx.blockLen; i++) {
            ctx.tmpblock[i] ^= ctx.c[i];
        }

        var k = (ctx.lastBlockSize != ctx.blockLen) ? ctx.k2 : ctx.k1;
        for (var i = 0; i < ctx.blockLen; i++) {
            ctx.tmpblock[i] ^= k[i];
        }

        ctx.encryptFunc(ctx.tmpblock, ctx.resimit, ctx.keys, ctx.printArray);
        arrCopy(outdata, ctx.resimit, 0, 0, ctx.s);
        return 0;
    };

    this.encryptCtr = this.cryptCtr;
    this.decryptCtr = this.cryptCtr;
    this.encryptOfb = this.cryptOfb;
    this.decryptOfb = this.cryptOfb;
}
