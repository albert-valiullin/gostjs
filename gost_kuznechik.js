/**
 * Алгоритм шифрования "Кузнечик"
 * @constructor
 */
function Gost15() {
    const blockLen = 16;
    /**
     * @returns {number} Длина блока в байтах.
     */
    this.getBlockLen = function() {
        return blockLen;
    };
    /**
     * Нелинейное биективное преобразование множества двоичных векторов. (Таблица перестановки)
     *
     * @type {number[]}
     * @const
     */
    var PI = [
        0xfc, 0xee, 0xdd, 0x11, 0xcf, 0x6e, 0x31, 0x16, 0xfb, 0xc4, 0xfa, 0xda, 0x23, 0xc5, 0x04, 0x4d,
        0xe9, 0x77, 0xf0, 0xdb, 0x93, 0x2e, 0x99, 0xba, 0x17, 0x36, 0xf1, 0xbb, 0x14, 0xcd, 0x5f, 0xc1,
        0xf9, 0x18, 0x65, 0x5a, 0xe2, 0x5c, 0xef, 0x21, 0x81, 0x1c, 0x3c, 0x42, 0x8b, 0x01, 0x8e, 0x4f,
        0x05, 0x84, 0x02, 0xae, 0xe3, 0x6a, 0x8f, 0xa0, 0x06, 0x0b, 0xed, 0x98, 0x7f, 0xd4, 0xd3, 0x1f,
        0xeb, 0x34, 0x2c, 0x51, 0xea, 0xc8, 0x48, 0xab, 0xf2, 0x2a, 0x68, 0xa2, 0xfd, 0x3a, 0xce, 0xcc,
        0xb5, 0x70, 0x0e, 0x56, 0x08, 0x0c, 0x76, 0x12, 0xbf, 0x72, 0x13, 0x47, 0x9c, 0xb7, 0x5d, 0x87,
        0x15, 0xa1, 0x96, 0x29, 0x10, 0x7b, 0x9a, 0xc7, 0xf3, 0x91, 0x78, 0x6f, 0x9d, 0x9e, 0xb2, 0xb1,
        0x32, 0x75, 0x19, 0x3d, 0xff, 0x35, 0x8a, 0x7e, 0x6d, 0x54, 0xc6, 0x80, 0xc3, 0xbd, 0x0d, 0x57,
        0xdf, 0xf5, 0x24, 0xa9, 0x3e, 0xa8, 0x43, 0xc9, 0xd7, 0x79, 0xd6, 0xf6, 0x7c, 0x22, 0xb9, 0x03,
        0xe0, 0x0f, 0xec, 0xde, 0x7a, 0x94, 0xb0, 0xbc, 0xdc, 0xe8, 0x28, 0x50, 0x4e, 0x33, 0x0a, 0x4a,
        0xa7, 0x97, 0x60, 0x73, 0x1e, 0x00, 0x62, 0x44, 0x1a, 0xb8, 0x38, 0x82, 0x64, 0x9f, 0x26, 0x41,
        0xad, 0x45, 0x46, 0x92, 0x27, 0x5e, 0x55, 0x2f, 0x8c, 0xa3, 0xa5, 0x7d, 0x69, 0xd5, 0x95, 0x3b,
        0x07, 0x58, 0xb3, 0x40, 0x86, 0xac, 0x1d, 0xf7, 0x30, 0x37, 0x6b, 0xe4, 0x88, 0xd9, 0xe7, 0x89,
        0xe1, 0x1b, 0x83, 0x49, 0x4c, 0x3f, 0xf8, 0xfe, 0x8d, 0x53, 0xaa, 0x90, 0xca, 0xd8, 0x85, 0x61,
        0x20, 0x71, 0x67, 0xa4, 0x2d, 0x2b, 0x09, 0x5b, 0xcb, 0x9b, 0x25, 0xd0, 0xbe, 0xe5, 0x6c, 0x52,
        0x59, 0xa6, 0x74, 0xd2, 0xe6, 0xf4, 0xb4, 0xc0, 0xd1, 0x66, 0xaf, 0xc2, 0x39, 0x4b, 0x63, 0xb6
    ];
    /**
     * Коэффициенты умножения в преобразовании L.
     *
     * @type {number[]}
     * @const
     */
    var C_L = [148, 32, 133, 16, 194, 192, 1, 251, 1, 192, 194, 16, 133, 32, 148, 1];
    /**
     * Преобразование X.
     * Сложение по модулю 2 двух двоичных векторов.
     *
     * @param {number[]}  a входной массив байт для преобразования
     * @param {number[]}  b входной массив байт для преобразования
     * @param {number[]} outdata выходной массив байт для результата преобразования
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var funcX = function(a, b, outdata, print) {
        if (!a || !b || !outdata) {
            if (print) {
                print("funcX: internal error!");
            }
            return -1;
        }

        for (var i = 0; i < blockLen; i++) {
            outdata[i] = a[i] ^ b[i];
        }

        if (print) {
            print("funcX: a: ", a);
            print("funcX: b: ", b);
            print("funcX: result: ", outdata);
        }
        return 0;
    };
    /**
     * Преобразование S.
     * Перестановка элементов двочного вектора с помощью массива {@link PI}.
     *
     * @param {number[]} a входной массив байт для преобразования
     * @param {number[]} outdata выходной массив байт для результата преобразования
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     *
     * @see PI
     */
    var funcS = function(a, outdata, print) {
        if (!a || !outdata) {
            if (print) {
                print("funcS: internal error!");
            }
            return -1;
        }

        for (var i = 0; i < blockLen; i++) {
            outdata[i] = PI[a[i]];
        }

        if (print) {
            print("funcS: input: ", a);
            print("funcS: output: ", outdata);
        }
        return 0;
    };
    /**
     * Обратное преобразование S.
     * Восстановление исходного двочного вектора с помощью массива {@link PI}.
     *
     * @param {number[]} a входной массив байт для преобразования
     * @param {number[]} outdata выходной массив байт для результата преобразования
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var funcReverseS = function(a, outdata, print) {
        if (!a || !outdata) {
            if (print) {
                print("funcReverseS: internal error!");
            }
            return -1;
        }

        for (var i = 0; i < blockLen; i++) {
            outdata[i] = PI.indexOf(a[i]);
        }

        if (print) {
            print("funcReverseS: input: ", a);
            print("funcReverseS: output: ", outdata);
        }
        return 0;
    };
    /**
     * Преобразование R.
     * Применение линейного преобразования в поле F с последующим сдвигом исходного вектора.
     *
     * @param {number[]} a входной массив байт для преобразования
     * @param {number[]} outdata выходной массив байт для результата преобразования
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var funcR = function(a, outdata, print) {
        if (!a || !outdata) {
            if (print) {
                print("funcR: internal error!");
            }
            return -1;
        }

        var sum = 0;
        for (var i = 0; i < blockLen; i++) {
            sum ^= MULT_TABLE[a[i] * 256 + C_L[i]];
        }

        outdata[0] = sum;
        arrCopy(outdata, a, 1, 0, blockLen - 1);

        for (var j = 1; j < blockLen; j++) {
            outdata[j] = a[j - 1];
        }

        if (print) {
            print("funcR: input: ", a);
            print("funcR: output: ", outdata);
        }
        return 0;
    };
    /**
     * Обратное преобразование R.
     * Применение линейного преобразования в поле F с последующим сдвигом исходного вектора.
     *
     * @param {number[]} a входной массив байт для преобразования
     * @param {number[]} outdata выходной массив байт для результата преобразования
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var funcReverseR = function(a, outdata, print) {
        if (!a || !outdata) {
            if (print) {
                print("funcReverseR: internal error!");
            }
            return -1;
        }

        var tmp = [];
        arrCopy(tmp, a, 0, 1, blockLen - 1);
        tmp[15] = a[0];

        var sum = 0;
        for (var i = 0; i < blockLen; i++) {
            sum ^= MULT_TABLE[tmp[i] * 256 + C_L[i]];
        }

        arrCopy(outdata, tmp, 0, 0, blockLen - 1);
        outdata[15] = sum;

        if (print) {
            print("funcReverseR: input: ", a);
            print("funcReverseR: output: ", outdata);
        }
        return 0;
    };
    /**
     * Преобразование L.
     * Последовательное применение преобразования R ({@link funcR}) 16 раз к исходному вектору.
     *
     * @param {number[]} a входной массив байт для преобразования
     * @param {number[]} outdata выходной массив байт для результата преобразования
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var funcL = function(a, outdata, print) {
        if (!a || !outdata) {
            if (print) {
                print("funcL: internal error!");
            }
            return -1;
        }

        var tmp = [];
        arrCopy(tmp, a, 0, 0, blockLen);

        for (var i = 0; i < blockLen; ++i) {
            funcR(tmp, outdata, print);
            arrCopy(tmp, outdata, 0, 0, blockLen);
        }

        if (print) {
            print("funcL: input: ", a);
            print("funcL: output: ", outdata);
        }
        return 0;
    };
    /**
     * Обратное преобразование L.
     * Последовательное применение преобразования обратное R ({@link funcReverseR}) 16 раз к исходному вектору.
     *
     * @param {number[]} a входной массив байт для преобразования
     * @param {number[]} outdata выходной массив байт для результата преобразования
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var funcReverseL = function(a, outdata, print) {
        if (!a || !outdata) {
            if (print) {
                print("funcReverseL: internal error!");
            }
            return -1;
        }

        var tmp = [];
        arrCopy(tmp, a, 0, 0, blockLen);

        for (var i = 0; i < blockLen; i++) {
            funcReverseR(tmp, outdata, print);
            arrCopy(tmp, outdata, 0, 0, blockLen);
        }

        if (print) {
            print("funcReverseL: input: ", a);
            print("funcReverseL: output: ", outdata);
        }
        return 0;
    };
    /**
     * Преобразование LSX.
     * Последовательное применение преобразований X ({@link funcX}), S ({@link funcS}), L ({@link funcL}).
     *
     * @param {number[]}  a входной массив байт для преобразования
     * @param {number[]}  b входной массив байт для преобразования
     * @param {number[]} outdata выходной массив байт для результата преобразования
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var funcLSX = function(a, b, outdata, print) {
        if (!a || !b || !outdata) {
            if (print) {
                print("funcLSX: internal error!");
            }
            return -1;
        }

        var temp1 = [];
        var temp2 = [];
        funcX(a, b, temp1, print);
        funcS(temp1, temp2, print);
        funcL(temp2, outdata, print);

        if (print) {
            print("funcLSX: a: ", a);
            print("funcLSX: b: ", b);
            print("funcLSX: output: ", outdata);
        }
        return 0;
    };
    /**
     * Обратное преобразование LSX.
     * Последовательное применение преобразований X ({@link funcX}), обратное L ({@link funcReverseL}), обратное S ({@link funcReverseS}).
     *
     * @param {number[]}  a входной массив байт для преобразования
     * @param {number[]}  b входной массив байт для преобразования
     * @param {number[]} outdata выходной массив байт для результата преобразования
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var funcReverseLSX = function(a, b, outdata, print) {
        if (!a || !b || !outdata) {
            if (print) {
                print("funcReverseLSX: internal error!");
            }
            return -1;
        }

        var temp1 = [];
        var temp2 = [];
        funcX(a, b, temp1, print);
        funcReverseL(temp1, temp2, print);
        funcReverseS(temp2, outdata, print);

        if (print) {
            print("funcReverseLSX: a: ", a);
            print("funcReverseLSX: b: ", b);
            print("funcReverseLSX: output: ", outdata);
        }
        return 0;
    };
    /**
     * Преобразование F.
     * Преобразование сети Фейсталя, участвующее в процедуре развертки ключа.
     *
     * @param {number[]} inputKey1 входной массив байт - первый ключ из пары ключей, полученной в предыдущей итерации
     * @param {number[]} inputKey2 входной массив байт - второй ключ из пары ключей, полученной в предыдущей итерации
     * @param {number[]} iterationConst входно   массив байт - итерационная костанта
     * @param {number[]} outputKey1 выходной массив байт - первый ключ
     * @param {number[]} outputKey2 выходной массив байт - второй ключ
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var funcF = function(inputKey1, inputKey2, iterationConst, outputKey1, outputKey2, print) {
        if (!inputKey1 || !inputKey2 || !iterationConst || !outputKey1 || !outputKey2) {
            if (print) {
                print("funcF: internal error!");
            }
            return -1;
        }

        var temp1 = [];
        var temp2 = [];
        funcLSX(inputKey1, iterationConst, temp1, print);
        funcX(temp1, inputKey2, temp2, print);

        arrCopy(outputKey2, inputKey1, 0, 0, blockLen);
        arrCopy(outputKey1, temp2, 0, 0, blockLen);

        if (print) {
            print("funcF: input key: ", inputKey1);
            print("funcF: input key: ", inputKey2);
            print("funcF: iteration const: ", iterationConst);
            print("funcF: output key: ", outputKey1);
            print("funcF: output key: ", outputKey2);
        }
        return 0;
    };
    /**
     * Вычисление итерационной константы C[i].
     * Используется для процедуры развертки ключа.
     *
     * @param {number} num номер константы
     * @param {number[]} output выходной массив байт - итерационная костанта
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    var funcC = function(num, output, print) {
        if (!output) {
            if (print)
                print("funcC: internal error!");
            return -1;
        }

        var tempI = [];
        for (var i = 0; i < blockLen - 1; i++) {
            tempI[i] = 0;
        }
        tempI[blockLen-1] = num;

        funcL(tempI, output, print);
        return 0;
    };
    /**
     * Процедура развертки ключа.
     *
     * @param {number[]} masterKey входной массив байт - мастер-ключ
     * @param {number[]} keys выходной массив байт - раундовые ключи
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.expandKey = function(masterKey, keys, print) {
        if (!masterKey || !keys) {
            if (print) {
                print("expandKey: internal error!");
            }
            return -1;
        }

        arrCopy(keys, masterKey, 0, 0, blockLen);
        arrCopy(keys, masterKey, blockLen, blockLen, blockLen);

        if (print) {
            print("expandKey: master key: ", masterKey);
            print("expandKey: output key: ", keys);
        }

        var c = [];
        var temp1 = [];
        var temp2 = [];
        for (var i = 0; i < 4; i++) {
            arrCopy(temp1, keys, 0, i * 2 * blockLen, blockLen);
            arrCopy(temp2, keys, 0, (i * 2 + 1) * blockLen, blockLen);

            for (var j = 1; j < 8; j++) {
                funcC(i * 8 + j, c, print);
                funcF(temp1, temp2, c, temp1, temp2, print);
            }

            funcC(i * 8 + 8, c, print);
            funcF(temp1, temp2, c, temp1, temp2, print);

            arrCopy(keys, temp1, (i * 2 + 2) * blockLen, 0, blockLen);
            arrCopy(keys, temp2, (i * 2 + 3) * blockLen, 0, blockLen);

            if (print) {
                print("expandKey: output key: ", keys.slice((i * 2 + 2) * blockLen, (i * 2 + 3) * blockLen));
                print("expandKey: output key: ", keys.slice((i * 2 + 3) * blockLen, (i * 2 + 4) * blockLen));
            }
        }
        return 0;
    };
    /**
     * Зашифрование блока.
     *
     * @param {number[]} plainText входной массив байт - блок данных для шифрования
     * @param {number[]} cipherText выходной массив байт - блок зашифрованных данных
     * @param {number[]} keys входной массив байт - раундовые ключи
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.encrypt = function(plainText, cipherText, keys, print) {
        if (!plainText || !cipherText || !keys) {
            if (print) {
                print("encrypt15: internal error!");
            }
            return -1;
        }

        var xTemp = [];
        var yTemp = [];
        arrCopy(xTemp, plainText, 0, 0, blockLen);

        for (var i = 0; i < 9; i++) {
            funcLSX(xTemp, keys.slice(blockLen * i, blockLen * (i + 1)), yTemp, print);
            arrCopy(xTemp, yTemp, 0, 0, blockLen);
        }
        funcX(yTemp, keys.slice(9 * blockLen, 10 * blockLen), cipherText, print);

        if (print) {
            print("encrypt15: key: ", keys);
            print("encrypt15: plain text: ", plainText);
            print("encrypt15: cipher text: ", cipherText);
        }
        return 0;
    };
    /**
     * Расшифрование блока.
     *
     * @param {number[]} cipherText входной массив байт - блок зашифрованных данных
     * @param {number[]} plainText выходной массив байт - блок расшифрованных данных
     * @param {number[]} keys входной массив байт - раундовые ключи
     * @param {function} [print] функция логирования
     * @returns {number} 0 - если преобразование прошло успешно, -1 - если произошла ошибка
     */
    this.decrypt = function(cipherText, plainText, keys, print) {
        if (!plainText || !cipherText || !keys) {
            if (print) {
                print("decrypt15: internal error!");
            }
            return -1;
        }

        var xTemp = [];
        var yTemp = [];
        arrCopy(xTemp, cipherText, 0, 0, blockLen);

        for (var i = 0; i < 9; i++) {
            funcReverseLSX(xTemp, keys.slice((9 - i) * blockLen, (9 - i + 1) * blockLen), yTemp, print);
            arrCopy(xTemp, yTemp, 0, 0, blockLen);
        }
        funcX(yTemp, keys, plainText, print);

        if (print) {
            print("decrypt15: key: ", keys);
            print("decrypt15: cipher text: ", cipherText);
            print("decrypt15: plain text: ", plainText);
        }
        return 0;
    };

    /////// alternative mult table value provider (generating values)
    // Mult table for R function
    var multTable = (function () {

        // Multiply two numbers in the GF(2^8) finite field defined
        // by the polynomial x^8 + x^7 + x^6 + x + 1 = 0 */
        function gmul(a, b) {
            var p = 0, counter, carry;
            for (counter = 0; counter < 8; counter++) {
                if (b & 1)
                    p ^= a;
                carry = a & 0x80; // detect if x^8 term is about to be generated
                a = (a << 1) & 0xff;
                if (carry)
                    a ^= 0xc3; // replace x^8 with x^7 + x^6 + x + 1
                b >>= 1;
            }
            return p & 0xff;
        }

        // It is required only this values for R function
        //       0   1   2    3    4    5    6    7
        var x = [1, 16, 32, 133, 148, 192, 194, 251];
        var m = [];
        for (var i = 0; i < 8; i++) {
            m[i] = [];
            for (var j = 0; j < 256; j++)
                m[i][j] = gmul(x[i], j);
        }
        return m;
    })();

    // 148, 32, 133, 16, 194, 192, 1, 251, 1, 192, 194, 16, 133, 32, 148, 1
    var kB = [4, 2, 3, 1, 6, 5, 0, 7, 0, 5, 6, 1, 3, 2, 4, 0];
}