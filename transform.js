function toByteArray(str) {
    var bytes = [];
    for (var i = 0; i < str.length; ++i) {
        bytes.push(Math.floor(str.charCodeAt(i) / 256));
        bytes.push(str.charCodeAt(i) % 256);
    }
    return bytes;
}

function toText(dataArray) {
    var str = "";
    for (var i = 0; i < dataArray.length; i += 2) {
        str += String.fromCharCode(dataArray[i] * 256 + dataArray[i+1]);
    }
    return str;
}
function toTextNormal(dataArray) {
    var str = "";
    for (var i = 0; i < dataArray.length; i += 2) {
        if (dataArray[i] + dataArray[i+1] == 0) {
            continue;
        }
        str += String.fromCharCode(dataArray[i] * 256 + dataArray[i+1]);
    }
    console.log(str);
    return str;
}

function blocks(dataArray) {
    var bls = [];
    var i,j,chunk = 16;
    for (i = 0, j = dataArray.length; i < j; i += chunk) {
        bls.push(dataArray.slice(i, i + chunk));
    }
    return bls;
}

function concatBlocks(blockArr) {
    var newArr = [];
    for(var i = 0; i < blockArr.length; i++) {
        newArr = newArr.concat(blockArr[i]);
    }
    return newArr;
}

function encryptText(txt) {
    var gost15 = new Gost15();
    var arr = toByteArray(txt);
    var arrBlocks = blocks(arr);
    var encBlocks = [];
    var keys = [];
    gost15.expandKey(masterKey15, keys);
    for(var i = 0; i < arrBlocks.length; i++) {
        var encBlock = [];
        gost15.encrypt(arrBlocks[i], encBlock, keys);
        encBlocks.push(encBlock);
    }
    var encArr = concatBlocks(encBlocks);
    return toText(encArr);
}

function decryptText(txt) {
    var gost15 = new Gost15();
    var arr = toByteArray(txt);
    var arrBlocks = blocks(arr);
    var decBlocks = [];
    var keys = [];
    gost15.expandKey(gost15, keys);
    for(var i = 0; i < arrBlocks.length; i++) {
        var encBlock = [];
        gost15.decrypt(arrBlocks[i], encBlock, keys);
        decBlocks.push(encBlock);
    }
    var encArr = concatBlocks(decBlocks);
    //return toTextAndDecode(encArr);
    return toTextNormal(encArr);
}

function toHexString(arr) {
    var res = "";
    for (var i = 0; i < arr.length; i++) {
        var hex = arr[i].toString(16);
        res += hex.length < 2 ? ("0"+hex) : hex;
    }
    return res;
}